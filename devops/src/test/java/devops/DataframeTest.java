package devops;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import devops.Dataframe;

import devops.Dataframe;
/**
 * Unit test for simple App.
 */
public class DataframeTest extends TestCase
{
    private Dataframe df;
	private final int DEFAULT_TIMEOUT = 5000;


    public DataframeTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( DataframeTest.class );
    }

    public void testEmptycreateFromListEmpty(){
        ArrayList<float[]> l = new  ArrayList<float[]>();
        String[] c = new String[1];
        c[0] = "colA";
        Dataframe df = new Dataframe(l, c , null);
        assertTrue("new list", df.isEmpty());
    }


	public void testSize() {
        ArrayList<float[]> li = new  ArrayList<float[]>();
        String[] c = new String[1];
        c[0] = "colA";
        String[] l = new String[1];
        l[0] = "lin1";
        float[] tmp = new float[1];
        tmp[0]= 0.1F;
        li.add(tmp);
        Dataframe f = new Dataframe(li, c, l);
        assertEquals("size Datafram with 1 element", false, f.isEmpty());
        Dataframe df = new Dataframe(null, c, l);
        assertEquals("size new empty Dataframe", true , df.isEmpty());
    }

	public void testSubDataframeByLine() {
        Dataframe f;Dataframe f2;Dataframe f3;Dataframe f4;
        String[] c = new String[2];
        c[0] = "colA"; c[1]="colB";
        String[] l = new String[2];
        l[0] = "lin1"; l[1]="lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0]= 0.1F; tmp0[1]=0.2F;
        float[] tmp1 = new float[2];
        tmp1[0]= 1.1F; tmp1[1]=1.2F;
        li.add(tmp0);
        li.add(tmp1);

        f = new Dataframe(li, c,l);

        //2eme Dataframe
        f2 = f.newFromLine(0,0); // de la ligne 0 a la ligne 0
        assertEquals("SubDataframe 0 0 colonne ok", f.getColumnName()[0], f2.getColumnName()[0]);
        assertEquals("SubDataframe 0 0 line ok", f.getLineName()[0], f2.getLineName()[0]);


        //OU
        int[] v = new int[1];
        v[0]=0; 
        f3 = f.newFromLine(v); // on veut le dataframe avec les lignes : 0
        assertEquals("SubDataframe from line 0 colonne ok", f.getLineName()[0], f3.getLineName()[0]);

        // OU
        boolean[] b = new boolean[2];
        b[0]=true; b[1]=false;
        f4 = f.newFromLine(b); // on veut le dataframe avec les lignes ou dans le tableau, cest a true
        assertEquals("SubDataframe from line 0 colonne ok", f.getLineName()[0], f4.getLineName()[0]);

    }

    public void testSubDataframebyColumn() {
        Dataframe f;Dataframe f2;Dataframe f3;Dataframe f4;
        String[] c = new String[2];
        c[0] = "colA"; c[1]="colB";
        String[] l = new String[2];
        l[0] = "lin1"; l[1]="lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0]= 0.1F; tmp0[1]=0.2F;
        float[] tmp1 = new float[2];
        tmp1[0]= 1.1F; tmp1[1]=1.2F;
        li.add(tmp0);
        li.add(tmp1);

        f = new Dataframe(li, c,l);

        //2eme Dataframe
        String[] col2 = new String[1]; col2[0]="lin1";
        f2 = f.newFromColumn(col2);
        assertEquals("SubDataframe 0 0 colonne ok", f.getColumnName()[0], f2.getColumnName()[0]);
        assertEquals("SubDataframe 0 0 line ok", f.getLineName()[0], f2.getLineName()[0]);
        assertEquals("SubDataframe 1  line ok", f.getLineName()[1], f2.getLineName()[1]);


        //OU
        int[] v = new int[1];
        v[0]=0;
        f3 = f.newFromColumn(v); // on veut le dataframe avec les lignes : 0
        assertEquals("SubDataframe from line 0 line ok", f.getLineName()[0], f3.getLineName()[0]);
        assertEquals("SubDataframe from line 0 line ok", f.getLineName()[1], f3.getLineName()[1]);
        assertEquals("SubDataframe from line 0 colonne ok", f.getColumnName()[0], f3.getColumnName()[0]);

        // OU
        boolean[] b = new boolean[2];
        b[0]=true; b[1]=false;
        f4 = f.newFromColumn(b); // on veut le dataframe avec les lignes ou dans le tableau, cest a true
        assertEquals("SubDataframe from line 0 line ok", f.getLineName()[0], f3.getLineName()[0]);
        assertEquals("SubDataframe from line 0 line ok", f.getLineName()[1], f3.getLineName()[1]);
        assertEquals("SubDataframe from line 0 colonne ok", f.getColumnName()[0], f3.getColumnName()[0]);


    }


    public void testminimums() {
        Dataframe f;
        String[] c = new String[2];
        c[0] = "colA";
        c[1] = "colB";
        String[] l = new String[2];
        l[0] = "lin1";
        l[1] = "lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0] = 0.1F;
        tmp0[1] = 0.2F;
        float[] tmp1 = new float[2];
        tmp1[0] = 1.1F;
        tmp1[1] = 1.2F;
        li.add(tmp0);
        li.add(tmp1);

        f = new Dataframe(li, c, l);
        assertEquals("minimum col 0",0.1F,f.min(0));
        assertEquals("minimum col 1",0.2F,f.min(1));

    }

    public void testmaximums() {
        Dataframe f;
        String[] c = new String[2];
        c[0] = "colA";
        c[1] = "colB";
        String[] l = new String[2];
        l[0] = "lin1";
        l[1] = "lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0] = 0.1F;
        tmp0[1] = 0.2F;
        float[] tmp1 = new float[2];
        tmp1[0] = 1.1F;
        tmp1[1] = 1.2F;
        li.add(tmp0);
        li.add(tmp1);

        f = new Dataframe(li, c, l);
        assertEquals("maximum col 0",1.1F,f.max(0));
        assertEquals("maximum col 1",1.2F,f.max(1));

    }

    public void testmeans() {
        Dataframe f;
        String[] c = new String[2];
        c[0] = "colA";
        c[1] = "colB";
        String[] l = new String[2];
        l[0] = "lin1";
        l[1] = "lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0] = 0.1F;
        tmp0[1] = 0.2F;
        float[] tmp1 = new float[2];
        tmp1[0] = 1.1F;
        tmp1[1] = 1.2F;
        li.add(tmp0);
        li.add(tmp1);

        f = new Dataframe(li, c, l);
        assertEquals("mean col 0",(0.1F+1.1F)/2,f.mean(0));
        assertEquals("mean col 1",(0.2F+1.2F)/2,f.mean(1));

    }

    public void testdfAbs() {
        Dataframe f;
        String[] c = new String[2];
        c[0] = "colA";
        c[1] = "colB";
        String[] l = new String[2];
        l[0] = "lin1";
        l[1] = "lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0] = -0.1F;
        tmp0[1] = 0.2F;
        float[] tmp1 = new float[2];
        tmp1[0] = 1.1F;
        tmp1[1] = -1.2F;
        li.add(tmp0);
        li.add(tmp1);

        f = new Dataframe(li, c, l);
        Dataframe f2 = f.abs();
        assertEquals("mean col 0",(0.1F+1.1F)/2,f2.mean(0));
        assertEquals("mean col 1",(0.2F+1.2F)/2,f2.mean(1));
        assertEquals("maximum col 0",1.1F,f2.max(0));
        assertEquals("maximum col 1",1.2F,f2.max(1));
        assertEquals("minimum col 0",0.1F,f2.min(0));
        assertEquals("minimum col 1",0.2F,f2.min(1));


    }


    public void testrenameColOrLine() {
        Dataframe f;
        String[] c = new String[2];
        c[0] = "colA";
        c[1] = "colB";
        String[] l = new String[2];
        l[0] = "lin1";
        l[1] = "lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0] = 0.1F;
        tmp0[1] = 0.2F;
        float[] tmp1 = new float[2];
        tmp1[0] = 1.1F;
        tmp1[1] = 1.2F;
        li.add(tmp0);
        li.add(tmp1);

        String[] newCol = new String[2]; newCol[0]="col1-1"; newCol[1]="col2-2";
        f = new Dataframe(li, c, l);
        f.renameLine("newlinename",0);
        f.renameColumn(newCol);
        assertEquals("colTot col 0","col1-1",f.getColumnName()[0]);
        assertEquals("colTot col 1","col2-2",f.getColumnName()[1]);
        f.renameColumn("",1);
        assertEquals("colUniQ col 0","col1-1",f.getColumnName()[0]);
        assertEquals("colUniQ col 1","",f.getColumnName()[1]);
        assertEquals("lineUniQ line 0","newlinename",f.getLineName()[0]);
        assertEquals("lineUniQ line 1","lin2",f.getLineName()[1]);


    }

    public void testgetLineOrColumn(){
        Dataframe f;
        String[] c = new String[2];
        c[0] = "colA";
        c[1] = "colB";
        String[] l = new String[2];
        l[0] = "lin1";
        l[1] = "lin2";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0] = 0.1F;
        tmp0[1] = 0.2F;
        float[] tmp1 = new float[2];
        tmp1[0] = 1.1F;
        tmp1[1] = 1.2F;
        li.add(tmp0);
        li.add(tmp1);
        f = new Dataframe(li, c, l);
    try {
        assertEquals("what is in line 0-0", 0.1F, f.getLine("lin1")[0]);
        assertEquals("what is in line 0-1", 0.2F, f.getLine("lin1")[1]);
        assertEquals("what is in line 1-0", 1.1F, f.getLine("lin2")[0]);
        assertEquals("what is in line 1-1", 1.2F, f.getLine("lin2")[1]);
    }catch(Exception e){

    }
        try {
            assertEquals("what is in column 0-0", 0.1F, f.getColumn("colA")[0]);
            assertEquals("what is in column 0-1", 0.2F, f.getColumn("colB")[0]);
            assertEquals("what is in column 1-0", 1.1F, f.getColumn("colA")[1]);
            assertEquals("what is in column 1-1", 1.2F, f.getColumn("colB")[1]);
        }catch(Exception e){

        }


    }

    public void testAppend(){
        Dataframe f;
        String[] c = new String[2];
        c[0] = "colA";
        c[1] = "colB";
        String[] l = new String[1];
        l[0] = "lin1";
        ArrayList<float[]> li = new ArrayList<float[]>();
        float[] tmp0 = new float[2];
        tmp0[0] = 0.1F;
        tmp0[1] = 0.2F;
        f = new Dataframe(li, c, l);
        float[] tmp1 = new float[2];
        tmp1[0] = 1.1F;
        tmp1[1] = 1.2F;
        li.add(tmp0);
        f.appendLine("newLine",tmp1);
        assertEquals("mean col 0",(0.1F+1.1F)/2,f.mean(0));
        assertEquals("mean col 1",(0.2F+1.2F)/2,f.mean(1));
        assertEquals("maximum col 0",1.1F,f.max(0));
        assertEquals("maximum col 1",1.2F,f.max(1));
        assertEquals("minimum col 0",0.1F,f.min(0));
        assertEquals("minimum col 1",0.2F,f.min(1));
        assertEquals("lineName 0","lin1" ,f.getLineName()[0] );
        assertEquals("lineName 1","newLine" ,f.getLineName()[1] );

    }
    public void testEmptycreateFromCSV(){
        assertTrue(true);//TODO
    }





}
