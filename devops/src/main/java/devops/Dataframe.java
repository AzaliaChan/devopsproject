package devops;
import java.util.ArrayList;
import java.util.Comparator;
import java.lang.Math;

public class Dataframe {
    private ArrayList<float[]> liste;
    private String[] columnName;
    private String[] lineName;

    /**
     * Create Dataframe using existing columns
     * @param l ArrayList of table of columns
     * @param colName the name of the columns
     * @param liName the name of the line, null if it have a generated name
     */
    public Dataframe(ArrayList<float[]> l, String[] colName, String[] liName){
        DataframeConstructor(l, colName, liName);
    }

    /**
     * Create Dataframe using CSV files
     * @param name of the CSV file
     * @param nameOfLineIncluded true if its true
     */
    public Dataframe(String name, boolean nameOfLineIncluded)throws java.io.IOException{
        FromCSV f = new FromCSV(name);
        String[] liName;
        String[] colName;
        ArrayList<float[]> l = (ArrayList) f.getList();
        if(!nameOfLineIncluded){
            liName = null;
        }else{
            liName = new String[l.size()];
            for(int i =0; i<l.size(); i++){
                liName[i]=""+l.get(i)[0];
            }
        }
        colName= new String[l.get(0).length];
        for(int i=0; i<l.get(0).length; i++){
            colName[i]=""+l.get(0)[i];
        }

        l.remove(0);
        DataframeConstructor(l,colName, liName );
    }

    private void DataframeConstructor(ArrayList<float[]> l, String[] colName, String[] liName){
        // Add the name of line
        if (liName== null){
            lineName= new String[l.size()];
            for (int i=0; i< lineName.length; i++)
                lineName[i]=""+i;
        }else{
            lineName = liName;
        }
        // Add the name of column
        columnName = colName;

        // Liste
        liste = l;
    }
    /**
     * Show all the Dataframe
     */
    public String showAll(){
        String s="\t";
        for (int i=0; i<columnName.length; i++){
            s+=columnName[i]+"\t";
        }
        for (int i=0; i<lineName.length; i++){
            s+="\n";
            s+=lineName[i]+"\t";
            for (int j=0; j<columnName.length; j++){
                s+=this.liste.get(i)[j]+"\t";
            }
        }
        System.out.println(s);
        return s;
    }

    /**
     * Show the n first lines
     * @param the number of line you want to show
     */
    public void showFirstLine(int n){
        String s="\t";
        for (int i=0; i<columnName.length; i++){
            s+=columnName[i]+"\t";
        }
        for (int i=0; i<n; i++){
            s+="\n";
            s+=lineName[i]+"\t";
            for (int j=0; j<columnName.length; j++){
                s+=liste.get(i)[j]+"\t";
            }
        }
    }


    /**
     * Show the n last lines
     * @param the number of line you want to show
     */
    public void showLastLine(int n)throws Exception{
            String s="\t";
            //afiche nom column
            for (int i=0; i<columnName.length; i++){
                s+=columnName[i]+"\t";
            }
            //Affiche les lignes
            for (int i=liste.size()-n; i<liste.size(); i++) {
                s += "\n";
                s += lineName[i] + "\t";
                for (int j = 0; j < columnName.length; j++) {
                    s += liste.get(i)[j] + "\t";
                }
            }
    }

    /**
     * Create a new Dataframe (thanks yo line) using the number of lines in the table
     * @param table of int, contaning the number of line you want
     * @return a Dataframe with the specific line you chose
     */
    public Dataframe newFromLine(int[] tab){
        ArrayList<float[]> l = new ArrayList<float[]>();
        String[] nameline = new String[tab.length];
        int j=0;
        for (int i: tab){
            l.add(this.liste.get(i));
            nameline[j] = lineName[i];
            j++;
        }
        return new Dataframe(l,columnName, nameline);
    }

    /**
     * Create a new Dataframe (thanks yo line) using a table
     * @param table of boolean, contaning if you want each line, size of the number of line of the Dataframe
     * @return a Dataframe with the specific line you chose
     */
    public Dataframe newFromLine(boolean[] tab){
        ArrayList<float[]> l = new ArrayList<float[]>();
        int sizeNewDataframe=0;
        for(int i=0; i<tab.length; i++){
            if(tab[i]){
                sizeNewDataframe++;
            }
        }
        String[] nameline = new String[sizeNewDataframe];
        int j=0;
        for (int i=0; i<liste.size(); i++){
            if(tab[i]) {
                l.add(this.liste.get(i));
                nameline[j] = lineName[i];
                j++;
            }
        }
        return new Dataframe(l,columnName, nameline);
    }

    /**
     * Create a new Dataframe (thanks yo line) using two int.
     * @param a, the first line you'll get
     * @param b, the last line you'll get
     * @return a Dataframe with the line starting at a to b
     */
    public Dataframe newFromLine(int a, int b){
        ArrayList<float[]> l = new ArrayList<float[]>();
        String[] nameline = new String[b-a+1];
        int j=0;
        for (int i=a; i<=b; i++){
            l.add(this.liste.get(i));
            nameline[j] = lineName[i];
            j++;
        }
        return new Dataframe(l,columnName, nameline);
    }

    /**
     * Create a new Dataframe (thanks to column) using the name of columns in the table
     * @param table of String, contaning the name of column you want
     * @return a Dataframe with the specific columns you chose
     */
    public Dataframe newFromColumn(String[] tab) {
        String[] namecol = new String[tab.length];
        int[] tab0 = new int[tab.length];
        int k=0;
        for(int i=0; i< tab.length; i++){
            for(int j=0; j< tab.length; j++){
                if(tab[i].equals(columnName[j])){
                    tab0[k]=j;
                }
            }
        }
        return newFromColumn(tab0);
    }

    /**
     * Create a new Dataframe (thanks to column) using the name of columns in the table
     * @param table of int, contaning the emplacement of column you want
     * @return a Dataframe with the specific columns you chose
     */
    public Dataframe newFromColumn(int[] tab) {
        ArrayList<float[]> l = new ArrayList<float[]>();
        String[] namecol = new String[tab.length];
        int j = 0;
        int m = 0;
        float[] res;
        for (int k : tab) {
            m=0;
            namecol[m] = columnName[k];
            m++;
        }
        m=0;
        //pour chaque ligne
        for (int i = 0; i < liste.size(); i++) {
            res = new float[tab.length];
            m = 0;
            //on remplit le nouveau tableau
            for (int k : tab) {
                res[m] = liste.get(i)[k];
                m++;
            }
            l.add(res);
            j++;
        }
        return new Dataframe(l, namecol, lineName);
    }

        /**
         * Create a new Dataframe (thanks to column) using the name of columns in the table
         * @param table of boolean, contaning if you want it, for each column , size of the number of column
         * @return a Dataframe with the specific columns you chose
         */
    public Dataframe newFromColumn(boolean[] tab){
        ArrayList<float[]> li = new ArrayList<float[]>();

        int sizeNewDataframe=0;
        for(int i=0; i<tab.length; i++){
            if(tab[i]){
                sizeNewDataframe++;
            }
        }
        int colI=0;
        String[] namecol = new String[sizeNewDataframe];
        for(int i=0; i<tab.length; i++){
            if(tab[i]){
                namecol[colI]=columnName[i];
                colI++;
            }
        }
        float[] res;
        int k=0;
        for(int i=0; i<liste.size(); i++){
            res=new float[namecol.length];
            k=0;
            for(int j=0; j<columnName.length; j++){
                if(tab[j]){
                    res[k]= liste.get(i)[j];
                    k++;
                }
            }
            li.add(res);
        }
        return new Dataframe(li, namecol, lineName);
    }

    /**
     * Show all stats about the dataframe
     */
    public void showStats(){
        ArrayList<ArrayList<Float>> l= new ArrayList<ArrayList<Float>>();
        for(int i=0; i< columnName.length; i++) l.add(new ArrayList<Float>());
        //l.get(i) -> colonne i
        //l.get(i).get(j) -> colonne j, ligne i
        float[] count= new float[columnName.length];
        float[] mean= new float[columnName.length]; for(int i=0; i<mean.length; i++) mean[i]=0;
        float[] mini= new float[columnName.length];
        float[] max= new float[columnName.length];
        float[] median= new float[columnName.length];
        float[] median25= new float[columnName.length];
        float[] median75= new float[columnName.length];

        for(int i=0; i<liste.size(); i++){
            for (int j = 0; j<liste.get(i).length; j++){
                l.get(j).add(liste.get(i)[j]);
                mean[j]+=liste.get(i)[j];
            }
        }
        for(int i=0; i<l.size(); i++){
            ArrayList<Float> l1= l.get(i);
            l1.sort(Comparator.naturalOrder());
            count[i]=l.get(i).size();
            mean[i]=mean[i]/count[i];
            mini[i]=l1.get(0);
            max[i]=l1.get(l1.size()-1);
            int index;
            if(((int)count[i])%2 == 0){
                median[i]=l1.get((int) (count[i]/2.0));
            }else{
                index = ((int)( count[i]-1.0))/2;
                index+=((int) (count[i]+1.0))/2;
                index=index/2;
                median[i]=(l1.get(index));
            }
            median25[i]=l1.get((int) Math.round(count[i]/4.0));
            median75[i]=l1.get((int) Math.round(3.0*count[i]/4.0));

        }
        String s="count:";
        for (int i=0; i< count.length; i++) s+=	"\t"+count[i];
        s+="\n"+"mean:";
        for (int i=0; i< mean.length; i++) s+=	"\t"+mean[i];
        s+="\n"+"mini:";
        for (int i=0; i< mini.length; i++) s+= "\t"+mini[i];
        s+="\n"+"max:";
        for (int i=0; i< max.length; i++) s+= "\t"+max[i];
        s+="\n"+"median:";
        for (int i=0; i< median.length; i++) s+= "\t"+median[i];
        s+="\n"+"median25:";
        for (int i=0; i< median25.length; i++) s+= "\t"+median25[i];
        s+="\n"+"median75:";
        for (int i=0; i< median75.length; i++) s+= "\t"+median75[i];
        System.out.println(s);
    }

    /**
     * minimum of one column
     * @param numero of column
     * @return the mini of the column given
     */
    public float min(int column){
        float[] mini=min();
        return mini[column];
    }

    /**
     * minimum of all column
     * @return table with min of each column
     */
    public float[] min(){
        float[] min= new float[columnName.length];
        for (int i=0; i<min.length;i++ ) min[i]= Float.MAX_VALUE;
        for (int i=0; i<liste.size();i++ ){
            for (int j=0; j<liste.get(i).length;j++ ){
                if(min[j]>liste.get(i)[j])
                    min[j]=liste.get(i)[j];
            }
        }
        return min;
    }

    /**
     * maximum of one column
     * @param numero of column
     * @return the max of the column given
     */
    public float max(int column){
        float[] maxi=max();
        return maxi[column];
    }
    /**
     * maximum of all column
     * @return table with max of each column
     */
    public float[] max(){
        float[] max= new float[columnName.length];
        for (int i=0; i<max.length;i++ ) max[i]= Float.MIN_VALUE;
        for (int i=0; i<liste.size();i++ ){
            for (int j=0; j<liste.get(i).length;j++ ){
                if(max[j]<liste.get(i)[j])
                    max[j]=liste.get(i)[j];
            }
        }
        return max;
    }

    /**
     * mean (average) of one column
     * @param numero of column
     * @return table with mean of  column given
     */
    public float mean(int column){
        float[] mean=mean();
        return mean[column];
    }
    /**
     * mean (average) of all column
     * @return table with mean of each column
     */
    public float[] mean(){
        float[] mean= new float[columnName.length];
        for (int i=0; i<mean.length;i++ ) mean[i]= 0;
        for (int i=0; i<liste.size();i++ ){
            for (int j=0; j<liste.get(i).length;j++ ){
                mean[j]+=liste.get(i)[j];
            }
        }
        for (int i=0; i<mean.length;i++ ) mean[i]=mean[i]/liste.size();
        return mean;
    }

    /**
     * is the dataframe empty
     * @return true if it is empty, else false
     */
    public boolean isEmpty(){
        if(liste==null || liste.isEmpty() || liste.get(0)==null )
            return true;
        return false;
    }

    /**
     * absolute of the dataframe
     * @return a new dataframe with each value >=0
     */
    public Dataframe abs(){
        ArrayList<float[]> l = new ArrayList<float[]>() ;
        float[] f;
        for (int i =0; i<liste.size(); i++){
            f = new float[liste.get(i).length];
            for (int j =0; j<liste.get(i).length; j++){
                float a=liste.get(i)[j];
                if(a<=0.0F)
                    f[j]=0.0F-a;
                else
                    f[j]=a;
            }
            l.add(f);
        }
        return new Dataframe(l, columnName, lineName);
    }

    /**
     * rename a line
     * @param new name of the line
     * @param numero of the Line
     */
    public void renameLine(String newname, int numLine){
        lineName[numLine]=newname;
    }
    /**
     * rename a column
     * @param new name of the column
     * @param numero of the column
     */
    public void renameColumn(String newname, int numCol){
            columnName[numCol]=newname;
    }

    /**
     * rename all column
     * @param table with the name of all column (in order)
     */
    public void renameColumn(String[] s){
        columnName=s;
    }

    /**
     * Add a new line to the dataframe
     * @param name of the new name. if null, replace by the number of the line
     * @param data to add
     */
    public void appendLine(String nameLine, float[] data){
        String[] f = new String[lineName.length+1];
        for(int i =0; i<f.length-1; i++){
            f[i]=lineName[i];
        }
        if(nameLine!=null)
            f[lineName.length]=nameLine;
        else
            f[lineName.length]=""+lineName.length;
        lineName=f;
        liste.add(data);
    }

    // GETTERS

    /**
     * get data of the given line
     * @param which line
     * @return data of the given line
     * @throws Exception
     */
    public float[] getLine(int n)throws Exception{
        return liste.get(n);
    }

    /**
     * get data of the given line
     * @param name of the line
     * @return data of the given line, if it didnt exist: null
     */
    public float[] getLine(String s) throws Exception{
        int i=0;
        while(i<lineName.length){
            if(lineName[i].equals(s)){
                return getLine(i);
            }
            i++;
        }
        return null ;
    }

    /**
     * name of the line
     * @return table with the name of the line
     */
    public String[] getLineName(){
        return lineName;
    }

    /**
     * name of the column
     * @return name of the column
     */
    public String[] getColumnName(){
        return columnName;
    }

    /**
     * Get data of a given column
     * @param name of the column
     * @return data of the column named s
     * @throws Exception
     */
    public float[] getColumn(String s)throws Exception{
        int i=0;
        while(i<columnName.length){
            if(columnName[i].equals(s)){
                return getColumn(i);
            }
            i++;
        }
        return null;
    }

    /**
     *
     * Get data of a given column
     * @param numero of the column
     * @return data of the n-th column
     * @throws Exception
     */
    public float[] getColumn(int n)throws Exception{
        float[] res = new float[liste.size()];
        for (int i=0; i<liste.size(); i++){
            res[i]= liste.get(i)[n];
        }
        return res;
    }


}
