package devops;
import java.util.List;
import java.io.FileReader;
import com.opencsv.CSVReader;
import java.io.IOException;

public class FromCSV{
    List<String[]> l;
    public FromCSV(String fileName) throws IOException{
        CSVReader reader = new CSVReader(new FileReader(fileName));
        l=reader.readAll();
    }
    public List<String[]> getList(){
        return l;
    }
}
